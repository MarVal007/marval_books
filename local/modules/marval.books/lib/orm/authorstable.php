<?php

namespace Marval\Books\Orm;

use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\StringField;

class AuthorsTable extends DataManager
{
    /**
     * Db table name
     *
     * @return string
     */
    public static function getTableName(): string
    {
        return 'marval_authors';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     * @throws \Bitrix\Main\SystemException
     */
    public static function getMap(): array
    {
        return [
            /**
             * ID
             */
            (new IntegerField('ID'))->configurePrimary()->configureAutocomplete(),

            /**
             * Author full name
             */
            (new StringField('FULL_NAME'))->configureRequired(),

            /**
             * Author country
             */
            (new StringField('Country')),
        ];
    }
}