<?php

namespace Marval\Books\Orm;

use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Fields\TextField;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\StringField;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Query\Join;

class BooksTable extends DataManager
{
    /**
     * Db table name
     *
     * @return string
     */
    public static function getTableName(): string
    {
        return 'marval_books';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     * @throws \Bitrix\Main\SystemException
     */
    public static function getMap(): array
    {
        return [
            /**
             * ID
             */
            (new IntegerField('ID'))->configurePrimary()->configureAutocomplete(),

            /**
             * Book name
             */
            (new StringField('NAME'))->configureRequired(),

            /**
             * Book description
             */
            (new TextField('DESCRIPTION')),

            /**
             * Files list
             */
            (new StringField('FILES', [
                'save_data_modification' => function () {
                    return array(
                        function ($value) {
                            if (is_array($value)) {
                                return json_encode($value, JSON_UNESCAPED_UNICODE);
                            } else {
                                return $value;
                            }

                        }
                    );
                },
                'fetch_data_modification' => function () {
                    return array(
                        function ($value) {
                            $arr = json_decode($value, true);
                            if (is_string($value) && is_array($arr)) {
                                return $arr;
                            } else {
                                return $value;
                            }

                        }
                    );
                }
            ])),

            /**
             * Book year
             */
            (new IntegerField('YEAR')),

            /**
             * Book price
             */
            (new IntegerField('PRICE'))->configureDefaultValue(0),

            /**
             * Author ID
             */
            (new IntegerField('AUTHOR'))->configureDefaultValue(0),
            (new Reference('AUTHOR', AuthorsTable::class, Join::on('this.AUTHOR', 'ref.ID')))->configureJoinType('INNER'),
        ];
    }
}