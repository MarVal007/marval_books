<?php

defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Grid;
use Bitrix\Main\SystemException;
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Main\UI\Filter;
use Bitrix\Main\Web\Uri;
use Marval\Books\Orm\BooksTable;
use Marval\Books\Orm\AuthorsTable;

Loc::loadMessages(__FILE__);
Loader::includeModule('admitad.core');

class CMarvalBookListComponent extends CBitrixComponent {
    public const GRID_ID = 'BOOKS_LIST';
    public const FILTERABLE_FIELDS = array(
            'YEAR',
            'AUTHOR'
    );

    private static $headers;
    private static $filterFields;
    private static $AUTHORS;


    /** @throws LoaderException */
    public function executeComponent() {
        try {
            Loader::requireModule('marval.books');
        } catch (LoaderException $e) {
            ShowError(Loc::getMessage('MARVAL_BOOKS_OUT_OF_MODULE'));
            return;
        }

        self::$AUTHORS = $this->getAuthors();

        self::$headers = [
            [
                'id' => 'NAME',
                'name' => Loc::getMessage('GRID_HEADER_NAME'),
                'default' => true,
                'width' => 150,
                'resizeable' => false,
            ],
            [
                'id' => 'DESCRIPTION',
                'name' => Loc::getMessage('GRID_HEADER_DESCRIPTION'),
                'default' => true,
                'width' => 150,
                'resizeable' => false,
            ],
            [
                'id' => 'YEAR',
                'name' => Loc::getMessage('GRID_HEADER_YEAR'),
                'default' => true,
                'width' => 150,
                'resizeable' => false,
            ],
            [
                'id' => 'PRICE',
                'name' => Loc::getMessage('GRID_HEADER_PRICE'),
                'default' => true,
                'width' => 150,
                'resizeable' => false,
            ],
            [
                'id' => 'AUTHOR',
                'name' => Loc::getMessage('GRID_HEADER_AUTHOR'),
                'default' => true,
                'width' => 150,
                'resizeable' => false,
            ],
        ];

        self::$filterFields = [
            [
                'id' => 'YEAR',
                'name' => Loc::getMessage('FILTER_YEAR'),
                'params' => array(
                    'multiple' => 'Y',
                ),
                'type' => 'list',
                'items' => $this->getYears(),
                'default' => true,
            ],
            [
                'id' => 'AUTHOR',
                'name' => Loc::getMessage('FILTER_AUTHOR'),
                'params' => array(
                    'multiple' => 'Y',
                ),
                'type' => 'list',
                'items' => self::$AUTHORS,
                'default' => true,
            ]
        ];

        $grid = new Grid\Options(self::GRID_ID);

        // region Filter.
        $gridFilter = new Filter\Options(self::GRID_ID, []);
        $gridFilterValues = $gridFilter->getFilter(self::$filterFields);
        $gridFilterValues = array_filter(
                $gridFilterValues,
                static function($fieldName) {
                    return in_array($fieldName, self::FILTERABLE_FIELDS, true);
                },
                ARRAY_FILTER_USE_KEY
        );

        // region Pagination.
        $gridNav = $grid->GetNavParams();
        $pager = new PageNavigation('');
        $pager->setPageSize($gridNav['nPageSize']);
        $recordCount = BooksTable::GetCount($gridFilterValues);
        $pager->setRecordCount($recordCount);
        if ($this->request->offsetExists('page')) {
            $currentPage = $this->request->get('page');
            $pager->setCurrentPage($currentPage > 0 ? $currentPage : $pager->getPageCount());
        } else {
            $pager->setCurrentPage(1);
        }
        // endregion

        $books = $this->getGridData(array(
            'filter' => $gridFilterValues,
            'limit' => $pager->getPageSize(),
            'offset' => $pager->getCurrentPage(),
        ));


        $requestUri = new Uri($this->request->getRequestedPage());
        $requestUri->addParams(array('sessid' => bitrix_sessid()));

        $this->arResult = array(
                'GRID_ID' => self::GRID_ID,
                'BOOKS' => $books,
                'HEADERS' => self::$headers,
                'FILTER' => self::$filterFields,
                'AJAX_FILTER' => $gridFilterValues,
                'PAGINATION' => array(
                        'PAGE_NUM' => $pager->getCurrentPage(),
                        'ENABLE_NEXT_PAGE' => $pager->getCurrentPage() < $pager->getPageCount(),
                        'URL' => $this->request->getRequestedPage(),
                ),
                'TOTAL_ROWS_COUNT' => $pager->getRecordCount(),
                'ENABLE_LIVE_SEARCH' => false,
                'DISABLE_SEARCH' => true,
                'SERVICE_URL' => $requestUri->getUri(),
        );

        $this->includeComponentTemplate();
    }

    private function getYears() {
        $years = [];
        $arr = BooksTable::getList(["select"=>["YEAR"]])->fetchAll();
        foreach ($arr as $item) {
            $years[$item["YEAR"]] = $item["YEAR"];
        }
        return array_unique($years);
    }

    private function getAuthors() {
        $result = [];
        $authors = AuthorsTable::getList()->fetchAll();
        foreach ($authors as $author) {
            $result[$author["ID"]] = $author["FULL_NAME"];
        }
        return $result;
    }

    private function getGridData($params) {
        $gridData = [];
        $books = BooksTable::getList($params)->fetchAll();
        foreach ($books as $book) {
            $gridData[] = [
                "NAME" => $book["NAME"],
                "DESCRIPTION" => $book["DESCRIPTION"],
                "YEAR" => $book["YEAR"],
                "PRICE" => $book["PRICE"],
                "AUTHOR" => self::$AUTHORS[$book["AUTHOR"]],
            ];
        }
        return $gridData;
    }
}
