<?php
$MESS['MARVAL_BOOKS_OUT_OF_MODULE'] = 'Модуль Книги не установлен';

$MESS['GRID_HEADER_NAME'] = 'Название';
$MESS['GRID_HEADER_DESCRIPTION'] = 'Описание';
$MESS['GRID_HEADER_YEAR'] = 'Год выхода';
$MESS['GRID_HEADER_PRICE'] = 'Цена';
$MESS['GRID_HEADER_AUTHOR'] = 'Автор';

$MESS['FILTER_YEAR'] = 'Год выхода';
$MESS['FILTER_AUTHOR'] = 'Автор';
