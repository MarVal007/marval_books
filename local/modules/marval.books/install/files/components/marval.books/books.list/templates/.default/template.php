<?php

defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;

Loc::loadMessages(__FILE__);

/** @var CMain $APPLICATION */
/** @var CBitrixComponentTemplate $this */
/** @var array $arResult */


$asset = Asset::getInstance();
$asset->addJs('/bitrix/js/crm/interface_grid.js');
$gridManagerId = $arResult['GRID_ID'] . '_MANAGER';
echo "<pre>"; var_dump($arResult);
$APPLICATION->IncludeComponent(
        'bitrix:crm.interface.grid',
        'titleflex',
        array(
                'GRID_ID' => $arResult['GRID_ID'],
                'HEADERS' => $arResult['HEADERS'],
                'ROWS' => $arResult['BOOKS'],
                'ALLOW_GROUP_ACTIONS' => true,
                'ALLOW_ROW_ACTIONS' => true,
                'AGGREGATE' => $arResult['AGGREGATE'],
                'PAGINATION' => $arResult['PAGINATION'],
                'FILTER' => $arResult['FILTER'],
                'FILTER_PRESETS' => $arResult['FILTER_PRESETS'],
                'IS_EXTERNAL_FILTER' => false,
                'ENABLE_LIVE_SEARCH' => $arResult['ENABLE_LIVE_SEARCH'],
                'DISABLE_SEARCH' => $arResult['DISABLE_SEARCH'],
                'ENABLE_ROW_COUNT_LOADER' => false,
                'AJAX_ID' => '',
                'AJAX_OPTION_JUMP' => 'N',
                'AJAX_OPTION_HISTORY' => 'N',
                'AJAX_LOADER' => null,
                'SHOW_MORE_BUTTON' => false,
                'SHOW_ROW_CHECKBOXES' => false,
                'SHOW_ROW_ACTIONS_MENU' => true,
                'SHOW_ACTION_PANEL' => false,
                'SHOW_PAGESIZE' => true,
                'SHOW_TOTAL_COUNTER' => true,
                'TOTAL_ROWS_COUNT' => isset($arResult['TOTAL_ROWS_COUNT']) ? (int) $arResult['TOTAL_ROWS_COUNT'] : null,
                'SHOW_SELECTED_COUNTER' => false,
                'SHOW_CHECK_ALL_CHECKBOXES' => false,
                'EXTENSION' => array(
                        'ID' => $gridManagerId,
                        'CONFIG' => array(
                                'ownerTypeName' => 'BOOK',
                                'gridId' => $arResult['GRID_ID'],
                                'serviceUrl' => $arResult['SERVICE_URL'],
                        ),
                ),
                'NAVIGATION_BAR' => array(),
        ),
        $this->getComponent(),
        array('HIDE_ICONS' => 'Y')
);
?>