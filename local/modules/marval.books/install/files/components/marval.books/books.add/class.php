<?php

use Marval\Books\Orm\BooksTable;
use Marval\Books\Orm\AuthorsTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

class CAddNewBookForm extends CBitrixComponent
{



    public function __construct($component = null)
    {
        parent::__construct($component);
        Loc::loadMessages(__FILE__);

        Loader::includeModule('marval.books');
    }

    public function executeComponent()
    {
        if($this->request->isAjaxRequest()) {
            $this->ajaxController($this->request->get('ajaxMethod'));
        }

        $this->includeComponentTemplate();
    }

    private function ajaxController($method) {
        global $APPLICATION;
        $APPLICATION->RestartBuffer();
        switch ($method) {
            case "saveBook":
                $this->saveBook();
                break;
            case "getAuthors":
                $this->getAuthors();
                break;
        }
        die();
    }

    private function saveBook() {
        $result = [];
        $files = $this->request->getFileList()->toArray();
        $fields = $this->request->getValues();
        $arrBook = [
            "NAME" => $fields["NAME"],
            "DESCRIPTION" => $fields["DESCRIPTION"],
            "YEAR" => $fields["YEAR"],
            "PRICE" => $fields["PRICE"],
            "AUTHOR" => $fields["AUTHOR"]
        ];
        $ids = [];
        foreach ($files["FILES"]["error"] as $key => $elem) {
            if($elem == 0) {
                $arrFile = [
                    "name" => $files["FILES"]["name"][$key],
                    "size" => $files["FILES"]["size"][$key],
                    "tmp_name" => $files["FILES"]["tmp_name"][$key],
                    "type" => $files["FILES"]["type"][$key],
                    "MODULE_ID" => "marval"
                ];
                $ids[] = CFile::SaveFile($arrFile, "marval_books");
            }
        }
        if(count($ids) > 0) {
            $arrBook["FILES"] = $ids;
        }
        $res = BooksTable::add($arrBook);
        echo json_encode($res->getId());
    }

    private function getAuthors() {
        //$authors = [["ID" => 11, "NAME" => 'Иванов'], ["ID" => 22, "NAME" => 'Петров'], ["ID" => 33, "NAME" => 'Сидоров']];
        $authors = AuthorsTable::getList()->fetchAll();
        echo json_encode($authors);
    }
}