<?php
$MESS ['FORM_TITLE'] = "Добавление новой книги";
$MESS ['BOOK_NAME'] = "Название";
$MESS ['BOOK_DESCRIPTION'] = "Краткое описание";
$MESS ['BOOK_YEAR'] = "Год выхода";
$MESS ['BOOK_PRICE'] = "Цена";
$MESS ['BOOK_AUTHOR'] = "Автор";
$MESS ['UPLOAD_FILE_BUTTON'] = "Добавить файл";
$MESS ['SAVE_BOOK'] = "Сохранить";
$MESS['REQUERIED_FIELDS_ERROR'] = "Не заполнены обязательные поля";
$MESS['SUCCESS_MESSAGE'] = "Книга добавлена";