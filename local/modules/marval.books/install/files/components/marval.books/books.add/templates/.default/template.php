<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\UI\Extension;

$APPLICATION->SetTitle(Loc::getMessage('FORM_TITLE'));

if($arResult["AJAX_ANSWER"]) {
    $APPLICATION->RestartBuffer();
    echo json_encode($arResult["AJAX_ANSWER"]);
    die();
}

Extension::load("ui.forms");
Extension::load("ui.alerts");
Extension::load("ui.buttons");

CJSCore::Init(["jquery"]);
?>
<form name="add_book" class="add_book-form" method="post" id="add_book_form">
    <div class="add_book-form-container">
        <div class="form-block">
            <div class="ui-alert ui-alert-success message-box" style="display: none" id="success_span">
                <span class="ui-alert-message"></span>
            </div>
            <div class="ui-alert ui-alert-danger message-box" style="display: none" id="error_span">
                <span class="ui-alert-message"></span>
            </div>
            <div class="ui-ctl-label-text"><?= Loc::getMessage('BOOK_NAME') ?></div>
            <div class="ui-ctl ui-ctl-textbox">
                <input name="NAME" type="text" class="ui-ctl-element" placeholder="<?= Loc::getMessage('BOOK_NAME') ?>">
            </div>
            <div class="ui-ctl-label-text"><?= Loc::getMessage('BOOK_DESCRIPTION') ?></div>
            <div class="ui-ctl ui-ctl-textarea">
                <textarea name="DESCRIPTION" class="ui-ctl-element"></textarea>
            </div>
            <label class="ui-ctl ui-ctl-file-btn">
                <input type="file" name="FILES[0]" class="ui-ctl-element" data-span="fspan0">
                <div class="ui-ctl-label-text"><?= Loc::getMessage('UPLOAD_FILE_BUTTON') ?></div>
                <span class="book_files_span fspan0"></span>
            </label>
            <label class="ui-ctl ui-ctl-file-btn">
                <input type="file" name="FILES[1]" class="ui-ctl-element" data-span="fspan1">
                <div class="ui-ctl-label-text"><?= Loc::getMessage('UPLOAD_FILE_BUTTON') ?></div>
                <span class="book_files_span fspan1"></span>
            </label>
            <div class="ui-ctl-label-text"><?= Loc::getMessage('BOOK_YEAR') ?></div>
            <div class="ui-ctl ui-ctl-textbox">
                <input name="YEAR" type="text" class="ui-ctl-element" placeholder="<?= Loc::getMessage('BOOK_YEAR') ?>">
            </div>
            <div class="ui-ctl-label-text"><?= Loc::getMessage('BOOK_PRICE') ?></div>
            <div class="ui-ctl ui-ctl-textbox">
                <input name="PRICE" type="text" class="ui-ctl-element" placeholder="<?= Loc::getMessage('BOOK_PRICE') ?>">
            </div>
            <div class="ui-ctl-label-text"><?= Loc::getMessage('BOOK_AUTHOR') ?></div>
            <div class="ui-ctl ui-ctl-after-icon ui-ctl-dropdown">
                <div class="ui-ctl-after ui-ctl-icon-angle"></div>
                <select name="AUTHOR" id="book_author" class="ui-ctl-element"></select>
            </div>
            <div class="ui-btn-split ui-btn-success">
                <button class="ui-btn-main" id="save_button" type="submit"><?=Loc::getMessage('SAVE_BOOK');?></button>
            </div>
        </div>
    </div>
</form>

<script>
    BX.JSparams = {
        bookForm: 'add_book_form',
        saveButton: 'save_button',
        authorsList: 'book_author',
        errorSpan: 'error_span',
        successSpan: 'success_span',
        requiredFields: <?= json_encode($arParams["REQUERIED_FIELDS"]);?>
    };
    BX.message({
        'REQUERIED_FIELDS_ERROR': '<?=Loc::getMessage('REQUERIED_FIELDS_ERROR');?>',
        'SUCCESS_MESSAGE': '<?=Loc::getMessage('SUCCESS_MESSAGE');?>',
    });
</script>