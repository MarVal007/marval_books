BX.ready(function() {
    /*
    Конечно список авторов можно было получать еще в class.php
    Но я хотел вообще сделать поле для поиска автора
    Но потом вохникло еще несколько вопросов по такой реализации, и я решил не плодить лишнего,
    но оставить функцию подгрузки списка авторов через AJAX
     */
    getAuthors();

    BX.bind(BX(BX.JSparams.bookForm), 'submit', function(e) {
        e.preventDefault();
        saveBook();
    });

    $("#"+BX.JSparams.bookForm+' input[type="file"]').on('change', function(){
        let filename = $(this).val().split(/[\\/]/g).pop();
        let fileSpan = $(this).data('span');
        $('.'+fileSpan).text(filename);
    });
});

function getAuthors() {
    let authors = [];
    let dataList = $('#'+BX.JSparams.authorsList);
    const bxFormData = new BX.ajax.FormData();

    bxFormData.append('ajaxMethod', 'getAuthors');

    bxFormData.send(
        document.location.href,
        function (response) {
            let result = JSON.parse(response);
            if(result.length > 0) {
                dataList.html('');
                result.forEach(function(item, i) {
                    dataList.append('<option value="'+item.ID+'">'+item.FULL_NAME+'</option>');
                });
            }
        },
        null,
        function(error){
            console.log(`error: ${error}`);
        }
    );
    return authors;
}

function saveBook() {
    const formData = new FormData(BX(BX.JSparams.bookForm));
    const bxFormData = new BX.ajax.FormData();

    $('#'+BX.JSparams.errorSpan).hide();
    $('#'+BX.JSparams.successSpan).hide();
    if(checkFields(formData)) {
        for(let [name, value] of formData)
        {
            bxFormData.append(name, value);
        }
        bxFormData.append('ajaxMethod', 'saveBook');

        bxFormData.send(
            document.location.href,
            function (response) {
                let result = JSON.parse(response);
                if(result) {
                    console.log(result);
                    $('#'+BX.JSparams.errorSpan).hide();
                    $('#'+BX.JSparams.successSpan + ' span').html(BX.message('SUCCESS_MESSAGE'));
                    $('#'+BX.JSparams.successSpan).show();
                }
            },
            null,
            function(error){
                console.log(`error: ${error}`);
            }
        );
    } else {
        showError(BX.message('REQUERIED_FIELDS_ERROR'));
    }

    $('#'+BX.JSparams.saveButton).removeClass('ui-btn-wait');
}

function checkFields(fields) {
    let emptyRequired = [];
    let requeriedFields = BX.JSparams.requiredFields;

    for(let [name, value] of fields)
    {
        if((!value) && (requeriedFields.indexOf(name) != -1)) {
            emptyRequired.push(name);
        }
    }
    return emptyRequired.length <= 0;
}

function showError(text) {
    $('#'+BX.JSparams.errorSpan+' span').html(text);
    $('#'+BX.JSparams.errorSpan).show();
}