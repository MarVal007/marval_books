<?php

use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Application;
use Bitrix\Main\SystemException;
use Bitrix\Main\Entity\Base;

Loc::loadMessages(__FILE__);

class marval_books extends CModule
{
    public function __construct()
    {
        if (file_exists(__DIR__ . "/version.php")) {
            $arModuleVersion = array();
            include_once(__DIR__ . "/version.php");

            $this->MODULE_ID = str_replace("_", ".", get_class($this));
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            $this->MODULE_NAME = Loc::getMessage("MARVAL_BOOKS_NAME");
            $this->MODULE_DESCRIPTION = Loc::getMessage("MARVAL_BOOKS_DESCRIPTION");

            $this->PARTNER_NAME = Loc::getMessage("MARVAL_BOOKS_PARTNER_NAME");
            $this->PARTNER_URI = Loc::getMessage("MARVAL_BOOKS_PARTNER_URI");
        }

        return false;
    }

    private function GetPath($notDocumentRoot = false)
    {
        if ($notDocumentRoot) {
            return str_ireplace(Application::getDocumentRoot(), '', dirname(__DIR__));
        } else {
            return dirname(__DIR__);
        }
    }

    private function isVersionD7()
    {
        if (CheckVersion(ModuleManager::getVersion('main'), '14.00.00')) {
            return true;
        } else {
            throw new SystemException(Loc::getMessage("MARVAL_BOOKS_INSTALL_ERROR_VERSION"));
        }
    }

    public function DoInstall()
    {
        global $APPLICATION;
        try {
            $this->isVersionD7();

            ModuleManager::registerModule($this->MODULE_ID);

            $this->InstallDB();

        } catch (Bitrix\Main\SystemException $exception) {
            global $EXSEPTION;
            $EXSEPTION = $exception;
        }
        finally {
            $APPLICATION->IncludeAdminFile(Loc::getMessage("MARVAL_BOOKS_INSTALL_TITLE"),
                $this->GetPath() . "/install/step.php");
        }
    }

    public function InstallDB()
    {
        Loader::includeModule($this->MODULE_ID);

        //Authors table
        if (!Application::getConnection(\Marval\Books\Orm\AuthorsTable::getConnectionName())->isTableExists(Base::getInstance('\Marval\Books\Orm\AuthorsTable')->getDBTableName())) {
            Base::getInstance('\Marval\Books\Orm\AuthorsTable')->createDbTable();
        }

        //Books table
        if (!Application::getConnection(\Marval\Books\Orm\BooksTable::getConnectionName())->isTableExists(Base::getInstance('\Marval\Books\Orm\BooksTable')->getDBTableName())) {
            Base::getInstance('\Marval\Books\Orm\BooksTable')->createDbTable();
        }

        return true;
    }

    public function DoUninstall()
    {
        global $APPLICATION;

        $this->UnInstallDB();

        ModuleManager::unRegisterModule($this->MODULE_ID);

        $APPLICATION->IncludeAdminFile(Loc::getMessage("MARVAL_BOOKS_INSTALL_TITLE"),
            $this->GetPath() . "/install/unstep.php");
    }

    public function UnInstallDB()
    {
        Loader::includeModule($this->MODULE_ID);

        //Books table
        Application::getConnection(\Marval\Books\Orm\BooksTable::getConnectionName())->queryExecute('drop table if exists ' . Base::getInstance('\Marval\Books\Orm\BooksTable')->getDBTableName());

        //Authors table
        Application::getConnection(\Marval\Books\Orm\AuthorsTable::getConnectionName())->queryExecute('drop table if exists ' . Base::getInstance('\Marval\Books\Orm\AuthorsTable')->getDBTableName());

        return true;
    }

    public function InstallFiles()
    {
        $root = Application::getDocumentRoot();

        CopyDirFiles(
            __DIR__.'/files/components/marval.books/',
            $root.'/local/components/marval.books/',
            true,
            true
        );
    }

    public function UnInstallFiles()
    {
        DeleteDirFilesEx('local/components/marval.books/');
    }

}